#include "game.hpp"
#include <stdexcept>
#include <string>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <typeinfo>

#include "play.hpp"

const char Card::CARD_RANK[15] = {'3', '4', '5', '6', '7', '8', '9', 'T',
                                  'J', 'Q', 'K',
                                  'A', '2',
                                  'r', 'R'};


int Card::map2Rank(char symbol) {
  for(unsigned int i=0; i<sizeof(CARD_RANK)/sizeof(char); i++) {
    if(CARD_RANK[i] == symbol) {
      return i;
    }
  }

  char buff[50];
  sprintf(buff, "Undefined card symbol %c", symbol);

  throw std::invalid_argument(buff);
}


Card::Card(char s, int mm) {
  rank = map2Rank(s);
  m = mm;
}


Card::Card(int r, int mm) {
  rank = r;
  m = mm;
}


Card::Card(const Card &c, int mm) {
  rank = c.rank;
  m = mm;
}


Card::Card(const Card &c) {
  rank = c.rank;
  m = c.m;
}


void Card::increase(int mm) {
  assert(mm>0);
  m += mm;
}

void Card::decrease(int mm) {
  assert(mm>0);
  m -= mm;
}


bool CardArray::operator == (const CardArray &rr) {
  if(this->size() != rr.size()) return false;

  int a1[15]={0}, a2[15]={0};
  for(CardArray::iterator it=this->begin(); it!=this->end(); ++it) {
    a1[it->getRank()] = it->getM();
  }

  for(CardArray::const_iterator it=rr.cbegin(); it!=rr.cend(); ++it) {
    a2[it->getRank()] = it->getM();
  }

  for(unsigned i=0; i<sizeof(a1)/sizeof(int); ++i) {
    if(a1[i]!=a2[i]) return false;
  }

  return true;
}


std::vector<int> CardArray::getRankVect() const {
  std::vector<int> rv;
  for(std::vector<Card>::const_iterator it=this->cbegin(); it!=this->cend(); ++it) {
    rv.push_back(it->getRank());
  }
  return rv;
}


Hand::Hand(unsigned int pid) : CardArray() {
  playerId = pid;
}


void CardArray::addCard(const Card &c) {
  for(CardArray::iterator it = this->begin(); it != this->end(); ++it) {
    if(it->sameRank(c)) {
      it->increase(c.getM());
      return;
    }
  }

  this->push_back(c);
}


void CardArray::addCard(char s, int mm) {
  addCard(Card(s, mm));
}


void CardArray::removeCard(const Card &c) {
  for(CardArray::iterator it = this->begin(); it != this->end(); ++it) {
    if(it->sameRank(c)) {
      it->decrease(c.getM());
      if(it->getM() == 0) {
        this->erase(it);
      }
      return;
    }
  }
}


void CardArray::removeCards(const CardArray &arr) {
  for(CardArray::const_iterator it = arr.begin(); it != arr.end(); ++it) {
    removeCard(*it);
  }
}


void CardArray::addCards(const CardArray &arr) {
  for(CardArray::const_iterator it = arr.begin(); it != arr.end(); ++it) {
    addCard(*it);
  }
}


Hand::Hand(const Hand &h) : CardArray(h) {
  playerId = h.playerId;
}


Hand::Hand(const CardArray &arr, int pid) : CardArray(arr) {
  playerId = pid;
}


std::string CardArray::print() {
  std::string str;

  for(CardArray::iterator it = this->begin(); it != this->end(); ++it) {
    char buff[20];
    sprintf(buff, "(%c,%d) ", it->getSymbol(), it->getM());
    str += buff;
  }

  return str;
}


bool CardRankCompareFunc(Card a, Card b) {
  return (a.getRank() < b.getRank());
}


void CardArray::sort() {
  std::sort(this->begin(), this->end(), CardRankCompareFunc);
}


unsigned int CardArray::totalNumOfCards() {
  unsigned int s = 0;
  for(CardArray::iterator it=this->begin(); it!=this->end(); ++it) {
    s += it->getM();
  }
  return s;
}


int charCmp(const void *p1, const void *p2) {
  return  *((char*)p1) > *((char*)p2);
}


CardArray CardArray::read(const char *s) {
  CardArray arr;

  char buffer[54];
  strcpy(buffer, s);
  int length = strlen(buffer);
  qsort(buffer, length, sizeof(char), charCmp);

  char c;
  for(int i=0; i<length; ) {
    c = buffer[i];
    int j = i+1;
    while(j<length && buffer[j]==c) {
      ++j;
    }
    arr.push_back(Card(c, j-i));
    i = j;
  }

  return arr;
}


#define mergeTwoVectors(a, b) a.insert(a.end(), b.begin(), b.end())

std::vector<PlayPattern*> Hand::searchMoves(PlayPattern *p) {
  std::vector<PlayPattern*> v0(p->next(*this));
  Bomb *b = dynamic_cast<Bomb*>(p);
  // If not a bomb, then all bombs can beat it
  if(b == 0) {
    std::vector<PlayPattern*> v1 = Bomb::allBombs(*this);
    mergeTwoVectors(v0,v1);
  }

  return v0;
}


std::vector<PlayPattern*> Hand::searchMoves() {
  std::vector<PlayPattern*> v0;
  std::vector<PlayPattern*> v1 = Solo::allSolos(*this);
  std::vector<PlayPattern*> v2 = Pair::allPairs(*this);
  std::vector<PlayPattern*> v3 = Trio::allTrios(*this);
  std::vector<PlayPattern*> v4 = Bomb::allBombs(*this);
  std::vector<PlayPattern*> v5 = SoloChain::allSoloChains(*this);
  std::vector<PlayPattern*> v6 = PairChain::allPairChains(*this);
  std::vector<PlayPattern*> v7 = TrioWithKicker::allTriosWithKicker(*this);
  std::vector<PlayPattern*> v8 = Airplane::allAirplanes(*this);
  std::vector<PlayPattern*> v9 = AirplaneWithKicker::allAirplanesWithKicker(*this);
  std::vector<PlayPattern*> v10 = FourWithDualSolo::allFourWithDualSolo(*this);
  std::vector<PlayPattern*> v11 = FourWithDualPair::allFourWithDualPair(*this);
  std::vector<PlayPattern*> v12 = Rocket::findRocket(*this);
  mergeTwoVectors(v0, v1);
  mergeTwoVectors(v0, v2);
  mergeTwoVectors(v0, v3);
  mergeTwoVectors(v0, v4);
  mergeTwoVectors(v0, v5);
  mergeTwoVectors(v0, v6);
  mergeTwoVectors(v0, v7);
  mergeTwoVectors(v0, v8);
  mergeTwoVectors(v0, v9);
  mergeTwoVectors(v0, v10);
  mergeTwoVectors(v0, v11);
  mergeTwoVectors(v0, v12);

  return v0;
}


int main1() {

  Hand h(0);
  h.addCard('4', 2);
  h.addCard('2', 2);
  h.addCard('A', 3);
  h.addCard('A', 1);
  h.addCard('K', 2);
  h.sort();
  std::cout << h.print() << std::endl;

  h.removeCard(Card(h[0],1));
  std::cout << h.print() << std::endl;

  const char *str = "32A2322";

  CardArray array = Hand::read(str);
  Hand h2(array, 0);
  std::cout << h2.print() << std::endl;

  return 0;
}
