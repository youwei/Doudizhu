#ifndef _GAME
#define _GAME

#include <vector>
#include <string>

class Card {
public:
  Card(char s, int mm=1);
  Card(int r, int mm=1);
  Card(const Card &c, int mm);
  Card(const Card &c);
  bool sameRank(const Card &c) const { return rank == c.rank; }
  char getSymbol() const { return CARD_RANK[rank]; }
  int getRank() const { return rank; }
  int getM() const { return m; }
  void increase(int mm=1);
  void decrease(int mm=1);
  bool operator == (const Card &c) {
    return (rank==c.rank && m==c.m);
  }

  static int map2Rank(char symbol);
protected:
  int rank;
  int m;

  static const char CARD_RANK[15];
};


class CardArray : public std::vector<Card> {
public:
  CardArray() : std::vector<Card>() {}
  CardArray(const CardArray &c) : std::vector<Card>(c) {}
  void addCard(const Card &c);
  void addCard(char s, int mm=1);
  void addCards(const CardArray &arr);
  void removeCard(const Card &c);
  void removeCards(const CardArray &arr);
  void sort();
  bool operator == (const CardArray &rr);
  std::string print();
  std::vector<int> getRankVect() const;
  // Return the total number of cards in a card array,
  // where each card rank may contain more than two suits and thus more than two cards
  unsigned int totalNumOfCards();
  
  static CardArray read(const char *s);
};


class PlayPattern;

class Hand : public CardArray {
public:
  Hand(unsigned int pid);
  Hand(const Hand &h);
  Hand(const CardArray &arr, int pid);
  // Find all possible next moves facing a play pattern from another person, excluding "PassPattern"
  std::vector<PlayPattern*> searchMoves(PlayPattern *p);
  // Find all possible moves with the hand of cards, excluding "PassPattern"
  std::vector<PlayPattern*> searchMoves();
  int getPlayerId() const { return playerId; }
protected:
  // Unique player id. Must be consecutive integer starting with 0
  // id 0 is conserved for landlord id
  unsigned int playerId;
};



#endif
