#include "mcts.hpp"
#include <cmath>
#include <vector>
#include <ctime>

UctNode::UctNode(UctNode* pa, int i, const std::vector<Hand*> &pt,
                 int begin, int current, const PlayPattern *prev, bool act) {
  paNode = pa;
  idx = i;
  for(unsigned int i=0; i<pt.size(); ++i) {
    playerPt.push_back(new Hand(*pt[i]));
  }

  numOfVisit = 0;
  beginPlayer = begin;
  currentPlayer = current;

  if(prev != NULL) {
    prevPattern = prev->clone(*playerPt[beginPlayer]);
  }
  else {
    prevPattern = NULL;
  }

  activated = act;
}


UctNode::~UctNode() {
  for(unsigned int i=0; i<playerPt.size(); ++i) {
    delete playerPt[i];
  }

  if(prevPattern != NULL) {
    delete prevPattern;
  }

  if(this->isLast() || this->isTerminal()) {
    return;
  }

  for(unsigned int i=0; i<chNode.size(); ++i) {
    delete chNode[i];
  }
}


void UctNode::addChNode(PlayPattern *pattern, int i) {
  Hand copyCurrent(*playerPt[currentPlayer]);
  PlayPattern *p = pattern->clone(copyCurrent);
  p->play();

  // If playing the pattern leads to the end of game, set the currentPlayer in the child node to -1
  int newCurrentPlayer = (currentPlayer+1)%playerPt.size();
  if(copyCurrent.totalNumOfCards()==0) {
    newCurrentPlayer = -1;
  }

  std::vector<Hand*> copyPlayerPt(playerPt);
  copyPlayerPt[copyCurrent.getPlayerId()] = &copyCurrent;

  chNode.push_back(new UctNode(this, i, copyPlayerPt, currentPlayer, newCurrentPlayer, p));
  delete p;

  chNumVisit.push_back(0);
  chScore.push_back(0.0);
}


void UctNode::addChNode(int i) {
  chNode.push_back(new UctNode(this, i, playerPt, beginPlayer, (currentPlayer+1)%playerPt.size(), prevPattern));

  chNumVisit.push_back(0);
  chScore.push_back(0.0);
}


UctNode* UctNode::expandCh() {
  // If the current player is also the beginning player, that is, no one beats him,
  // child nodes consist of all possible moves.
  // Otherwise, child nodes are those corresponding to compatible patterns that can beat the current pattern
  std::vector<PlayPattern*> *possiblePatterns;
  if(currentPlayer == beginPlayer) {
    possiblePatterns = new std::vector<PlayPattern*>(playerPt[currentPlayer]->searchMoves());
  }
  else {
    possiblePatterns = new std::vector<PlayPattern*>(playerPt[currentPlayer]->searchMoves(prevPattern));
  }

  int i=0;
  for(std::vector<PlayPattern*>::iterator it=possiblePatterns->begin(); it != possiblePatterns->end(); ++it, ++i) {
    // add the node resulting from a non-pass action pattern performed on the current player
    addChNode(*it, i);
    delete *it;
  }

  // add the node corresponding to the pass action if the current player receives a pattern
  if(currentPlayer != beginPlayer) {
    addChNode(i);
  }

  delete possiblePatterns;

  return this->chNode[rand()%this->chNode.size()];
}


std::vector<double> UctNode::getUctScore() {
  std::vector<double> v(chScore.size());
  for(unsigned int i=0; i<chScore.size(); ++i) {
    v[i] = chScore[i] + UCT_PARAM*sqrt( log(numOfVisit*1.0) / (chNumVisit[i]+1e-5) );
  }
  return v;
}



void UctNode::select(UctNode **lastNode, int *level) {
  UctNode *pt = this;
  int ll = 0;

  while(!pt->isTerminal() && !pt->isLast()) {
    // First increase the number of visit to the selected node before running uct algorithm
    // After selecting a child node, increase the count of selected child node
    ++ pt->numOfVisit;
    int chNo = pt->selectChNode();
    ++ pt->chNumVisit[chNo];

    pt = pt->chNode[chNo];
    ++ll;
  }
  *lastNode = pt;
  *level = ll;
}


int UctNode::simulate(GameSimulatorAbstractFactory &simulatorFactory) {
  if(isTerminal()) {
    return beginPlayer;
  }
   // static int i=0;

   // if(i%10000==0) std::cout << "Simulation: " << i << std::endl;
   // i++;

   GameSimulator *s = simulatorFactory.createGameSimulator(playerPt, beginPlayer, currentPlayer, prevPattern);
   int result = s->run();
   delete s;
   return result;
}


int UctNode::selectChNode() {
  double max=-1e100;
  int max_i=-1, tieN=1;
  std::vector<double> uctScore = getUctScore();


  for(unsigned int i=0 ; i < uctScore.size(); ++i) {
    double a = uctScore[i];

    // If a tie, uniformly break it
    if(fabs(max-a)<1e-2) {
      ++tieN;
      if(rand()%tieN == 0) {
        max_i = i;
      }
      continue;
    }

    if(max < a) {
      max_i = i;
      max = a;
      tieN = 1;
    }
  }

  return max_i;
}


void UctNode::backpropagate(int winner) {
  UctNode *node = this->paNode;
  int idx = this->idx;

  do {
    int reward;
    if(node->currentPlayer==winner || (winner>0 && node->currentPlayer>0)) {
      reward = 1;
    }
    else {
      reward = 0;
    }

    node->chScore[idx] = (node->chScore[idx]*(node->chNumVisit[idx]-1)+reward)/node->chNumVisit[idx];

    idx = node->idx;
    node = node->paNode;
  }
  while(node != NULL);
}


SearchTree::SearchTree(GameSimulatorAbstractFactory &f, const std::vector<Hand*> &pt,
		       int begin, int current, const PlayPattern *prev)
  : simulatorFactory(f) {
  root = new UctNode(NULL, -1, pt, begin, current, prev, true);
}


void SearchTree::estimate(int maxN) {
  for(int i=0; i<maxN; ++i) {
    // Select
    UctNode *lastNode;
    int level;
    root->select(&lastNode, &level);

    int winner;
    if(!lastNode->isTerminal()) {

      // Expand
      if(level < MAX_HEIGHT && lastNode->isActivated()) {
	lastNode->expandCh();
	lastNode->select(&lastNode);
	++level;
      }

      // Simulate
      winner = lastNode->simulate(simulatorFactory);
      if(!lastNode->isActivated()) {
	lastNode->setActivated();
      }
    }
    else {
      winner = lastNode->getWinner();
    }

    // Backpropagation
    lastNode->backpropagate(winner);

    if(i%(maxN/10)==0) {
      for(unsigned int j=0; j<root->chNode.size(); ++j) {
        std::cout << root->chNode[j]->prevPattern->print() << root->chScore[j] << '\n';
      }
      std::cout << std::endl;
    }
  }
}


int main() {
  Hand ll(0), p1(1), p2(2);

  /**************************
  ll.addCard('4', 2);
  ll.addCard('2', 1);

  p1.addCard('4', 2);
  p1.addCard('6', 3);
  p1.addCard('5', 2);

  p2.addCard('5', 2);
  **************************/

  /*************************************/
  /* 一个斗地主残局盘面，解法第一步要出顺子 */
  /*************************************/
  ll.addCard('4', 1);
  ll.addCard('7', 2);
  ll.addCard('9', 1);
  ll.addCard('T', 2);
  ll.addCard('J', 1);
  ll.addCard('Q', 1);
  ll.addCard('K', 1);


  p1.addCard('3', 1);
  p1.addCard('5', 1);
  p1.addCard('6', 1);
  p1.addCard('7', 1);
  p1.addCard('8', 2);
  p1.addCard('J', 1);
  p1.addCard('Q', 1);
  p1.addCard('K', 2);
  p1.addCard('A', 2);
  p1.addCard('2', 1);

  p2.addCard('3', 1);
  /***************************/
  

  std::vector<Hand*> pt;
  pt.push_back(&ll);
  pt.push_back(&p1);
  //pt.push_back(&p2);

  srand(time(0));
  UniformSimulatorFactory factory;
  SearchTree st(factory, pt, 0, 0);
  st.estimate(500000);

  return 0;
}
