#ifndef _MCTS
#define _MCTS

#include <vector>
#include "game.hpp"
#include "simulator.hpp"

#define MAX_HEIGHT 100

#define UCT_PARAM (1.414213562) // sqrt(2)

class SearchTree;


class UctNode {
  friend class SearchTree;
public:
  UctNode(UctNode* pa, int i, const std::vector<Hand*> &pt,
	  int begin=0, int current=0, const PlayPattern *prev=NULL, bool act=false);

  UctNode(const UctNode &s);
  
  virtual ~UctNode();

  // Expand child nodes corresponding to possible moves (both non-pass and pass moves) by the current player,
  // and return an arbitrarily selected child node if there are multiple child nodes
  UctNode* expandCh();

  // Check if the node is terminal
  bool isTerminal() const { return currentPlayer<0; }

  // Check if has reached the last node of the current path, which may be terminal or not terminal
  // Note if not the last node, at least there should be one child node for the pass action
  bool isLast() const { return chNode.size()==0; }

  bool isActivated() const { return activated; }
  void setActivated() { activated = true; }

  // Return the winner of the game if the node is terminal
  int getWinner() const { return (isTerminal()? beginPlayer : -1); }

  int getIdx() const { return idx; }

  // Select a path according to some tree policy, e.g. uct algorithm,
  // and return the last node of the path for following expansion or backup propagation
  virtual void select(UctNode **lastNode, int *level);

  virtual void select(UctNode **lastNode) {
    int temp;
    select(lastNode, &temp);
  }

  int simulate(GameSimulatorAbstractFactory &simulatorFactory);

  void backpropagate(int winner);
  
protected:
  std::vector<UctNode*> chNode;
  std::vector<int> chNumVisit;

  // Scores of child nodes in the view of the current player.
  // For landlord, it is his empirical mean of winning probability;
  // for each peasant, it is the empirical probability of landlord losing the game
  std::vector<double> chScore;

  // pointer to the parent node
  UctNode *paNode;

  // the index of the current node in its parent, convenient for backpropagation
  int idx;

  // number of visits to this node
  int numOfVisit;

  std::vector<Hand*> playerPt;

  // The player who starts a round 
  int beginPlayer;

  // The player who is about to play or follow cards next
  int currentPlayer;

  // Tha play pattern for the current player to beat or follow, which is passed from the previous "non-pass" player
  PlayPattern *prevPattern;

  int selectChNode();

  // flag of whether expanding child nodes to this node is allowed
  // After performing a simulation from the current node, it is activated and allowed to expand its child nodes
  bool activated;
private:
  // add child node corresponding to some action beating the previous
  // i: the index of the child node in its parent
  void addChNode(PlayPattern *pattern, int i);

  // add child node corresponding to pass action. The prevPattern is passed on to the child node
  // i: the index of the child node in its parent
  void addChNode(int i);

  std::vector<double> getUctScore();
};


class SearchTree {
public:
  SearchTree(GameSimulatorAbstractFactory &f, const std::vector<Hand*> &pt,
	     int begin=0, int current=0, const PlayPattern *prev=NULL);
  ~SearchTree() {
    delete root;
  }

  void estimate(int maxN=1000000);

protected:
  UctNode *root;
  GameSimulatorAbstractFactory &simulatorFactory;
};

#endif
