#include "play.hpp"
#include <iostream>
#include <cassert>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <string>

ChainInfo::ChainInfo(int bb, int ee) {
  beginRank = bb;
  endRank = ee;
}

ChainInfo::ChainInfo(const ChainInfo &c) {
  beginRank = c.beginRank;
  endRank = c.endRank;
}

void PlayPattern::play() {
  CardArray arr = this->expand();
  source.removeCards(arr);
}

std::string PlayPattern::print() {
  std::string str;
  char tmp[20];
  sprintf(tmp, "palyer %d: ", source.getPlayerId());
  str += tmp;
  str += this->expand().print();
  return str;
}

bool PlayPattern::uniqueCards(const CardArray &arr) {
  bool a[15]={false};
  for(CardArray::const_iterator it=arr.cbegin(); it!=arr.cend(); ++it) {
    if(a[it->getRank()] == true) {
      return false;
    }
    else {
      a[it->getRank()] = true;
    }
  }
  return true;
}


std::vector<CardArray> PlayPattern::findUniqueCards(const CardArray &arr, unsigned int n, int m, const std::vector<int> &excludedRanks) {

  if(arr.size() < n) {
    return std::vector<CardArray>();
  }

  if(n==1) {
    std::vector<CardArray> results;
    for(CardArray::const_iterator it=arr.cbegin(); it!=arr.cend(); ++it) {
      // If it has no enough multiplicities or should be excluded, then skip it
      if(it->getM() < m || std::find(excludedRanks.cbegin(), excludedRanks.cend(), it->getRank()) != excludedRanks.cend()) {
        continue;
      }

      CardArray arr;
      arr.push_back(Card(it->getRank(), m));
      results.push_back(arr);
    }
    return results;
  }
  else if(n==2) {
    std::vector<CardArray> results;
    for(CardArray::const_iterator ii=arr.cbegin(); ii!=arr.cend(); ++ii) {

      if(ii->getM() < m || std::find(excludedRanks.cbegin(), excludedRanks.cend(), ii->getRank()) != excludedRanks.cend()) {
        continue;
      }

      for(CardArray::const_iterator jj=ii+1; jj!=arr.cend(); ++jj) {
        if(jj->getM() < m || std::find(excludedRanks.cbegin(), excludedRanks.cend(), jj->getRank()) != excludedRanks.cend()) {
          continue;
        }

        CardArray arr;
        arr.push_back(Card(ii->getRank(), m));
        arr.push_back(Card(jj->getRank(), m));
        results.push_back(arr);
      }
    }
    return results;
  }
  else {
    int n1=n/2-1, n2=n%2;
    std::vector<CardArray> r1 = findUniqueCards(arr, 2, m, excludedRanks), r2;

    while(n1>0) {
      for(std::vector<CardArray>::iterator it=r1.begin(); it!=r1.end(); ++it) {
        std::vector<int> ex(excludedRanks);
        std::vector<int> pr = it->getRankVect();
        ex.insert(ex.end(), pr.begin(), pr.end());

        std::vector<CardArray> b = findUniqueCards(arr, 2, m, ex);
        for(std::vector<CardArray>::iterator jt=b.begin(); jt!=b.end(); ++jt) {
          jt->addCards(*it);
          r2.push_back(*jt);
        }
      }
      r1.swap(r2);
      r2.clear();
      --n1;
    };

    if(n2==1) {
      for(std::vector<CardArray>::iterator it=r1.begin(); it!=r1.end(); ++it) {
        std::vector<int> ex(excludedRanks);
        std::vector<int> pr = it->getRankVect();
        ex.insert(ex.end(), pr.begin(), pr.end());

        std::vector<CardArray> b = findUniqueCards(arr, 1, m, ex);
        for(std::vector<CardArray>::iterator jt=b.begin(); jt!=b.end(); ++jt) {
          jt->addCards(*it);
          r2.push_back(*jt);
        }
      }
      r1.swap(r2);
      r2.clear();
    }

    std::vector<CardArray> results;
    for(std::vector<CardArray>::iterator it=r1.begin(); it!=r1.end(); ++it) {
      it->sort();

      bool flag=true;
      for(std::vector<CardArray>::iterator jt=results.begin(); jt!=results.end(); ++jt) {
        if(*it == *jt) {
          flag = false;
          break;
        }
      }
      if(flag) {
        results.push_back(*it);
      }
    }
    
    return results;
  }
}



std::vector<ChainInfo> PlayPattern::longestChains(const CardArray &arr, unsigned int minimalLength, int m) {
  unsigned a[15]={0}; // a[i] keeps the length of the chain beginning from the card with rank i
  std::vector<ChainInfo> results;
  Card c2('2');
  
  for(CardArray::const_iterator it=arr.cbegin(); it!=arr.cend(); ++it) {
    int i = it->getRank();

    if(i >= c2.getRank()) break; // exclude 2 and Jokers

    if( it->getM() >= m ) {
      a[i] = 1;
    }
    else {
      continue;
    }


    int j = i-1;
    while(j>=0 && a[j]>0) {
      if(a[j]>0) {
        a[j]++;
        --j;
      }
      else {
        break;
      }
    }
  }
  
  for(unsigned i=0; i<sizeof(a)/sizeof(unsigned); ) {
    if(a[i] < minimalLength) {
      ++i;
      continue;
    }

    results.push_back(ChainInfo(i, i+a[i]-1));
    i += a[i];
  }

  return results;
}


SoloChain::SoloChain(Hand &s, const Card &ss, const Card &ee) : PlayPattern(s)  {
  Card ll('3'), uu('A');
  assert(ss.getRank() >= ll.getRank() && ee.getRank() <= uu.getRank());

  beginRank = ss.getRank();
  endRank = ee.getRank() + 1;

  assert(endRank-beginRank >= 5);
  length = endRank - beginRank;
}



CardArray SoloChain::expand() const {
  CardArray arr;
  for(int i=beginRank; i<endRank; ++i) {
    arr.push_back(Card(i, 1));
  }
  return arr;
}


PairChain::PairChain(Hand &s, const Card &ss, const Card &ee) : PlayPattern(s) {
  Card ll('3'), uu('A');
  assert(ss.getRank() >= ll.getRank() && ee.getRank() <= uu.getRank());

  beginRank = ss.getRank();
  endRank = ee.getRank() + 1;

  assert(endRank-beginRank >= 3);
  length = endRank - beginRank;
}


CardArray PairChain::expand() const {
  CardArray arr;
  for(int i=beginRank; i<endRank; ++i) {
    arr.push_back(Card(i, 2));
  }
  return arr;
}


TrioWithKicker::TrioWithKicker(Hand &s, const Card &master, const Card &k, bool flag) : Trio(s, master){
  kickerRank = k.getRank();
  pairKicker = flag;
}


CardArray TrioWithKicker::expand() const {
  CardArray arr;
  arr.push_back(Card(rank, 3));
  if(doubleKicker()) {
    arr.push_back(Card(kickerRank, 2));
  }
  else {
    arr.push_back(Card(kickerRank, 1));
  }
  return arr;
}



Airplane::Airplane(Hand &s, const Card &ss, const Card &ee) : PlayPattern(s){
  beginRank = ss.getRank();
  endRank = ee.getRank()+1;
  length = endRank-beginRank;
  assert(length>=2);
}


Airplane::Airplane(const Airplane &c) : PlayPattern(c.source) {
  beginRank = c.beginRank;
  endRank = c.endRank;
  length = c.length;
}


CardArray Airplane::expand() const {
  CardArray arr;
  for(int i=beginRank; i<endRank; ++i) {
    arr.push_back(Card(i, 3));
  }
  return arr;
}


AirplaneWithKicker::AirplaneWithKicker(Hand &s, const Card &ss, const Card &ee, const CardArray &kk, bool largeKickerFlag) : Airplane(s, ss, ee) {
  largeKicker = largeKickerFlag;
  for(CardArray::const_iterator it=kk.cbegin(); it!=kk.cend(); ++it) {
    if(!largeKickerFlag) {
      kickers.push_back(Card(*it, 1));
    }
    else {
      kickers.push_back(Card(*it, 2));
    }
  }
}

AirplaneWithKicker::AirplaneWithKicker(const Airplane &a, const CardArray &kk) : Airplane(a), kickers(kk) {
  largeKicker = true;
  for(CardArray::iterator it=kickers.begin(); it!=kickers.end(); ++it) {
    if(it->getM() == 1) {
      largeKicker = false;
      break;
    }
  }
}


CardArray AirplaneWithKicker::expand() const {
  CardArray arr(Airplane::expand());
  arr.insert(arr.end(), kickers.cbegin(), kickers.cend());
  return arr;
}


FourWithDualSolo::FourWithDualSolo(Hand &s, const Card &ss, const Card &k1, const Card &k2) : PlayPattern(s) {
  rank = ss.getRank();
  kickerRank[0] = k1.getRank();
  kickerRank[1] = k2.getRank();
  assert(kickerRank[0] != kickerRank[1]);
}


FourWithDualSolo::FourWithDualSolo(Hand &s, const Card &ss, const Card &k) : PlayPattern(s) {
  rank = ss.getRank();
  kickerRank[0] = k.getRank();
  kickerRank[1] = k.getRank();
}


CardArray FourWithDualSolo::expand() const {
  CardArray arr;
  arr.push_back(Card(rank, 4));
  if(kickerRank[0]==kickerRank[1]) {
    arr.push_back(Card(kickerRank[0], 2));
  }
  else {
    arr.push_back(Card(kickerRank[0], 1));
    arr.push_back(Card(kickerRank[1], 1));
  }
  return arr;
}


FourWithDualPair::FourWithDualPair(Hand &s, const Card &ss, const Card &k1, const Card &k2) : PlayPattern(s) {
  rank = ss.getRank();
  kickerRank[0] = k1.getRank();
  kickerRank[1] = k2.getRank();
}


CardArray FourWithDualPair::expand() const {
  CardArray arr;
  arr.push_back(Card(rank, 4));
  arr.push_back(Card(kickerRank[0], 2));
  arr.push_back(Card(kickerRank[1], 2));
  return arr;
}



SpaceShuttle::SpaceShuttle(Hand &s, const Card &ss, const Card &ee) : PlayPattern(s){
  beginRank = ss.getRank();
  endRank = ee.getRank()+1;
  length = endRank-beginRank;
  assert(length>=2);
}



CardArray SpaceShuttle::expand() const {
  CardArray arr;
  for(int i=beginRank; i<endRank; ++i) {
    arr.push_back(Card(i, 4));
  }
  return arr;
}


SpaceShuttleWithKicker::SpaceShuttleWithKicker(Hand &s, const Card &ss, const Card &ee, const CardArray &arr) : SpaceShuttle(s, ss, ee) {
  assert(arr.size()==this->length || arr.size()==this->length*2);
  assert(PlayPattern::uniqueCards(arr));

  for(CardArray::const_iterator it=arr.cbegin(); it!=arr.cend(); ++it) {
    if(arr.size() == this->length) {
      kickers.push_back(Card(*it, 2));
    }
    else {
      kickers.push_back(Card(*it, 1));
    }
  }
}


CardArray SpaceShuttleWithKicker::expand() const {
    CardArray arr(SpaceShuttle::expand());
    arr.insert(arr.end(), kickers.cbegin(), kickers.cend());
    return arr;
}


CardArray Rocket::expand() const {
  CardArray arr;
  arr.push_back(Card('r'));
  arr.push_back(Card('R'));
  return arr;
}



std::vector<PlayPattern*> Solo::allSolos(Hand &h) {
  std::vector<PlayPattern*> v;
  for(CardArray::iterator it=h.begin(); it!=h.end(); ++it) {
    assert(it->getM()>0);
    v.push_back(new Solo(h, *it));
  }
  return v;
}


std::vector<PlayPattern*> Pair::allPairs(Hand &h) {
  std::vector<PlayPattern*> v;
  for(CardArray::iterator it=h.begin(); it!=h.end(); ++it) {
    assert(it->getM()>0);
    if(it->getM() >= 2) {
      v.push_back(new Pair(h, *it));
    }
  }
  return v;
}


std::vector<PlayPattern*> Trio::allTrios(Hand &h) {
  std::vector<PlayPattern*> v;
  for(CardArray::iterator it=h.begin(); it!=h.end(); ++it) {
    assert(it->getM()>0);
    if(it->getM() >= 3) {
      v.push_back(new Trio(h, *it));
    }
  }
  return v;
}


std::vector<PlayPattern*> Bomb::allBombs(Hand &h) {
  std::vector<PlayPattern*> v;
  for(CardArray::iterator it=h.begin(); it!=h.end(); ++it) {
    assert(it->getM()>0);
    if(it->getM() == 4) {
      v.push_back(new Bomb(h, *it));
    }
  }
  return v;
}


std::vector<ChainInfo> PlayPattern::allChains(Hand &h, int minimalLength, int m) {
  std::vector<ChainInfo> v;

  std::vector<ChainInfo> cs = PlayPattern::longestChains(h, minimalLength, m);
  for(std::vector<ChainInfo>::iterator it=cs.begin(); it!=cs.end(); ++it) {
    int ll = it->endRank - it->beginRank + 1;
    assert(ll>=minimalLength);
    for(int i=0; i<=ll-minimalLength; ++i) {
      int length = i+minimalLength;

      for(int j=0; j<=ll-length; ++j) {
	int bb = it->beginRank + j;
	int ee = bb + length -1;
        v.push_back(ChainInfo(bb, ee));
      }
    }
  }
  return v;

}


std::vector<PlayPattern*> SoloChain::allSoloChains(Hand &h) {
  std::vector<PlayPattern*> v;

  std::vector<ChainInfo> cs = PlayPattern::allChains(h, 5, 1);
  for(std::vector<ChainInfo>::iterator it=cs.begin(); it!=cs.end(); ++it) {
    v.push_back(new SoloChain(h, Card(it->beginRank), Card(it->endRank)));
  }
  return v;
}


std::vector<PlayPattern*> PairChain::allPairChains(Hand &h) {
  std::vector<PlayPattern*> v;

  std::vector<ChainInfo> cs = PlayPattern::allChains(h, 3, 2);
  for(std::vector<ChainInfo>::iterator it=cs.begin(); it!=cs.end(); ++it) {
    v.push_back(new PairChain(h, Card(it->beginRank), Card(it->endRank)));
  }
  return v;
}


std::vector<PlayPattern*> TrioWithKicker::allTriosWithKicker(Hand &h) {
  std::vector<PlayPattern*> v;

  std::vector<PlayPattern *> t=Trio::allTrios(h);
  std::vector<PlayPattern *> s=Solo::allSolos(h);
  std::vector<PlayPattern *> p=Pair::allPairs(h);

  Trio *tp;
  Solo *sp;
  Pair *pp;
  for(std::vector<PlayPattern*>::iterator it=t.begin(); it!=t.end(); ++it) {
    tp =  dynamic_cast<Trio*>(*it);
    for(std::vector<PlayPattern*>::iterator is=s.begin(); is!=s.end(); ++is) {
      sp = dynamic_cast<Solo*>(*is);
      if(tp->getRank() != sp->getRank()) {
        v.push_back(new TrioWithKicker(h, Card(tp->getRank()), Card(sp->getRank())));
      }
    }

    for(std::vector<PlayPattern*>::iterator ip=p.begin(); ip!=p.end(); ++ip) {
      pp = dynamic_cast<Pair*>(*ip);
      if(tp->getRank() != pp->getRank()) {
        v.push_back(new TrioWithKicker(h, Card(tp->getRank()), Card(pp->getRank()), true));
      }
    }    
  }

  PlayPattern::freePatternsInVector(t);
  PlayPattern::freePatternsInVector(s);
  PlayPattern::freePatternsInVector(p);

  return v;
}


std::vector<PlayPattern*> Airplane::allAirplanes(Hand &h) {
  std::vector<PlayPattern*> v;

  std::vector<ChainInfo> cs = PlayPattern::allChains(h, 2, 3);
  for(std::vector<ChainInfo>::iterator it=cs.begin(); it!=cs.end(); ++it) {
    v.push_back(new Airplane(h, Card(it->beginRank), Card(it->endRank)));
  }

  return v;
}


std::vector<PlayPattern*> AirplaneWithKicker::allAirplanesWithKicker(Hand &h) {
  std::vector<PlayPattern*> v;
  std::vector<PlayPattern*> a = Airplane::allAirplanes(h);

  for(std::vector<PlayPattern*>::iterator it=a.begin(); it!=a.end(); ++it) {
    Airplane *ap = dynamic_cast<Airplane*>(*it);
    std::vector<int> m = ap->getRankVect();

    // Find small wings (unique solo cards)
    std::vector<CardArray> sw = PlayPattern::findUniqueCards(h, ap->getLength(), 1, m);
    for(std::vector<CardArray>::iterator jt=sw.begin(); jt!=sw.end(); ++jt) {
      v.push_back(new AirplaneWithKicker(*ap, *jt));
    }

    // Find large wings (pair cards)
    std::vector<CardArray> lw = PlayPattern::findUniqueCards(h, ap->getLength(), 2, m);
    for(std::vector<CardArray>::iterator pt=lw.begin(); pt!=lw.end(); ++pt) {
      v.push_back(new AirplaneWithKicker(*ap, *pt));
    }

    // Find small wings (two cards of the same rank viewed as two separate kicker cards)
    // e.g. 33344477, 444555666778.
    if(h.totalNumOfCards() - m.size()*3 < m.size()) {
      continue;
    }

    for(unsigned int i=1; i<=m.size()/2; ++i) {
      // i is the number of pairs; m.size()-i*2 is the number of unique solo cards
      int numOfSolos = m.size()-i*2;
	
      std::vector<CardArray> pw = PlayPattern::findUniqueCards(h, i, 2, m);

      if( numOfSolos == 0 ) {	
	for(unsigned int j=0; j<pw.size(); ++j) {
	  CardArray kickers = pw[j];
	  v.push_back(new AirplaneWithKicker(*ap, kickers));
	}
      }
      else {
	for(unsigned int j=0; j<pw.size(); ++j) {
	  CardArray kickers = pw[j];

	  std::vector<int> mm(m);
	  std::vector<int> rr = pw[j].getRankVect();
	  mm.insert(mm.end(), rr.begin(), rr.end());

	  std::vector<CardArray> ppw = PlayPattern::findUniqueCards(h, numOfSolos, 1, mm);
	  for(unsigned int jj=0; jj<ppw.size(); ++jj) {
	    kickers.addCards(ppw[jj]);
	    v.push_back(new AirplaneWithKicker(*ap, kickers));
	    kickers.removeCards(ppw[jj]);
	  }
	}
      }
    }    
  }

  PlayPattern::freePatternsInVector(a);

  return v;
}


std::vector<PlayPattern*> FourWithDualSolo::allFourWithDualSolo(Hand &h) {
  std::vector<PlayPattern*> v;
  std::vector<PlayPattern*> a = Bomb::allBombs(h);

  for(std::vector<PlayPattern*>::iterator it=a.begin(); it!=a.end(); ++it) {
    Bomb *bp=dynamic_cast<Bomb*>(*it);
    int rank = bp->getRank();
    std::vector<int> ex;
    ex.push_back(rank);

    // Find two distinct cards
    std::vector<CardArray> dc = PlayPattern::findUniqueCards(h, 2, 1, ex);
    for(std::vector<CardArray>::iterator jt=dc.begin(); jt!=dc.end(); ++jt) {
      v.push_back(new FourWithDualSolo(h, Card(rank), (*jt)[0], (*jt)[1]));
    }

    // Find a pair
    std::vector<CardArray> pc = PlayPattern::findUniqueCards(h, 1, 2, ex);
    for(std::vector<CardArray>::iterator kt=pc.begin(); kt!=pc.end(); ++kt) {
      assert(kt->size()==1);
      v.push_back(new FourWithDualSolo(h, Card(rank), (*kt)[0]));
    }
  }
  
  PlayPattern::freePatternsInVector(a);
  return v;
}


std::vector<PlayPattern*> FourWithDualPair::allFourWithDualPair(Hand &h) {
  std::vector<PlayPattern*> v;
  std::vector<PlayPattern*> a = Bomb::allBombs(h);

  for(std::vector<PlayPattern*>::iterator it=a.begin(); it!=a.end(); ++it) {
    int rank = dynamic_cast<Bomb*>(*it)->getRank();
    std::vector<int> ex;
    ex.push_back(rank);

    // Find two distinct pairs
    std::vector<CardArray> sc = PlayPattern::findUniqueCards(h, 2, 2, ex);
    for(std::vector<CardArray>::iterator rt=sc.begin(); rt!=sc.end(); ++rt) {
      v.push_back(new FourWithDualPair(h, Card(rank), (*rt)[0], (*rt)[1]));
    }
  }

  PlayPattern::freePatternsInVector(a);
  return v;
}


std::vector<PlayPattern*> Rocket::findRocket(Hand &h) {
  std::vector<PlayPattern*> v;
  if(h.size()<2) return v;

  char x = h.back().getSymbol();
  char y = h[h.size()-2].getSymbol();

  if(x=='R' && y=='r') {
    v.push_back(new Rocket(h));
  }

  return v;

}


std::vector<PlayPattern*> PlayPattern::next(Hand &h)  {
  std::vector<PlayPattern*> v=Rocket::findRocket(h);
  return v;
}


std::vector<PlayPattern*> Solo::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = Solo::allSolos(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    if(dynamic_cast<Solo*>(*it)->getRank() > rank) {
      v.push_back(*it);
    }
    else {
      delete *it;
    }
  }
  return v;
}


std::vector<PlayPattern*> Pair::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = Pair::allPairs(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    if(dynamic_cast<Pair*>(*it)->getRank() > rank) {
      v.push_back(*it);
    }
    else {
      delete *it;
    }
  }
  return v;
}


std::vector<PlayPattern*> Trio::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = Trio::allTrios(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    if(dynamic_cast<Trio*>(*it)->getRank() > rank) {
      v.push_back(*it);
    }
    else {
      delete *it;
    }
  }
  return v;
}


std::vector<PlayPattern*> Bomb::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = Bomb::allBombs(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    if(dynamic_cast<Bomb*>(*it)->getRank() > rank) {
      v.push_back(*it);
    }
    else {
      delete *it;
    }
  }
  return v;
}


std::vector<PlayPattern*> SoloChain::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = SoloChain::allSoloChains(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    SoloChain *p = dynamic_cast<SoloChain*>(*it);
    if(length==p->getLength() && p->getBeginRank() > beginRank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}


std::vector<PlayPattern*> PairChain::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = PairChain::allPairChains(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    PairChain *p = dynamic_cast<PairChain*>(*it);
    if(length==p->getLength() && p->getBeginRank() > beginRank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}


std::vector<PlayPattern*> TrioWithKicker::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = TrioWithKicker::allTriosWithKicker(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    TrioWithKicker *p = dynamic_cast<TrioWithKicker*>(*it);
    if(this->singleKicker()==p->singleKicker() && p->getRank() > rank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}


std::vector<PlayPattern*> Airplane::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = Airplane::allAirplanes(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    Airplane *p = dynamic_cast<Airplane*>(*it);
    if(this->getLength()==p->getLength() && p->getBeginRank() > beginRank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}


std::vector<PlayPattern*> AirplaneWithKicker::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = AirplaneWithKicker::allAirplanesWithKicker(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    AirplaneWithKicker *p = dynamic_cast<AirplaneWithKicker*>(*it);
    if(this->getLength()==p->getLength() && largeKicker==p->hasLargeKicker()
       && p->getBeginRank() > beginRank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}


std::vector<PlayPattern*> FourWithDualSolo::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = FourWithDualSolo::allFourWithDualSolo(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    FourWithDualSolo *p = dynamic_cast<FourWithDualSolo*>(*it);
    if(p->getRank() > rank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}


std::vector<PlayPattern*> FourWithDualPair::next(Hand &h) {
  std::vector<PlayPattern*> v=PlayPattern::next(h);
  std::vector<PlayPattern*> c = FourWithDualPair::allFourWithDualPair(h);
  for(std::vector<PlayPattern*>::iterator it=c.begin(); it!=c.end(); ++it) {
    FourWithDualPair *p = dynamic_cast<FourWithDualPair*>(*it);
    if(p->getRank() > rank) {
      v.push_back(p);
    }
    else {
      delete p;
    }
  }
  return v;
}



int main2() {

  Hand h10(0);
  h10.addCard('3', 3);
  h10.addCard('4', 4);
  h10.addCard('5', 2);
  h10.addCard('6', 3);
  h10.addCard('7', 3);
  h10.addCard('8', 3);
  h10.addCard('R', 1);
  std::vector<PlayPattern *> aa = SoloChain::allSoloChains(h10);
  assert(aa.size()==3);
  std::cout << aa[0]->print() << " | " << aa[1]->print() << " | "  << aa[2]->print()  << std::endl;
  PlayPattern::freePatternsInVector(aa);

  std::vector<PlayPattern *> bb = PairChain::allPairChains(h10);
  assert(bb.size()==10);
  std::cout << bb[0]->print() << " | " << bb[1]->print() << " | "  << bb[2]->print() << "|"  << bb[3]->print() << " | " << bb[4]->print() << " | " << bb[5]->print() << " | " << bb[6]->print() <<  " | " << bb[7]->print() << "\n\n";
  PlayPattern::freePatternsInVector(bb);

  std::vector<PlayPattern*> tk = TrioWithKicker::allTriosWithKicker(h10);
  for(unsigned i=0; i<tk.size(); i++) {
    std::cout << tk[i]->print() << " | " ;
  }
  PlayPattern::freePatternsInVector(tk);

  std::vector<PlayPattern*> ak = Airplane::allAirplanes(h10);
  for(unsigned i=0; i<ak.size(); i++) {
    std::cout << ak[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(ak);


  Hand h11(0);
  h11.addCard('3', 3);
  h11.addCard('4', 3);
  h11.addCard('5', 3);
  h11.addCard('6', 2);
  h11.addCard('7', 1);
  h11.addCard('8', 1);
  h11.addCard('R', 1);
  std::cout << h11.print() << std::endl;

  std::vector<PlayPattern*> apk = AirplaneWithKicker::allAirplanesWithKicker(h11);
  for(unsigned i=0; i<apk.size(); i++) {
    std::cout << apk[i]->print() << "\n " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(apk);

  Hand h12(0);
  h12.addCard('3', 4);
  h12.addCard('4', 3);
  h12.addCard('5', 4);
  h12.addCard('6', 1);
  h12.addCard('7', 2);
  h12.addCard('8', 2);
  h12.addCard('r', 1);
  h12.addCard('R', 1);
  std::cout << h12.print() << std::endl;

  std::vector<PlayPattern*> fds = FourWithDualSolo::allFourWithDualSolo(h12);
  for(unsigned i=0; i<fds.size(); i++) {
    std::cout << fds[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(fds);

  std::vector<PlayPattern*> fdp = FourWithDualPair::allFourWithDualPair(h12);
  for(unsigned i=0; i<fdp.size(); i++) {
    std::cout << fdp[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(fdp);

  std::vector<PlayPattern*> rrr = Rocket::findRocket(h12);
  assert(rrr.size()==1);
  PlayPattern::freePatternsInVector(rrr);

  Hand x0(0);
  x0.addCard('4', 3);
  x0.addCard('5', 4);
  x0.addCard('6', 1);
  x0.addCard('7', 2);
  x0.addCard('8', 2);
  x0.addCard('r', 1);
  x0.addCard('R', 1);


  Hand x1(0);
  x1.addCard('3', 4);
  x1.addCard('4', 3);
  x1.addCard('5', 1);
  x1.addCard('6', 1);
  x1.addCard('7', 2);

  SoloChain s0(x1, Card('3'), Card('7'));

  std::vector<PlayPattern*> nxt = s0.next(x0);
  for(unsigned i=0; i<nxt.size(); ++i) {
    std::cout << nxt[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt);

  std::vector<PlayPattern*> nxt1 = x0.searchMoves(&s0);
  for(unsigned i=0; i<nxt1.size(); ++i) {
    std::cout << nxt1[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt1);

  Solo s1(x1, Card('3'));
  std::vector<PlayPattern*> nxt2 = x0.searchMoves(&s1);
  for(unsigned i=0; i<nxt2.size(); ++i) {
    std::cout << nxt2[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt2);

  Pair s2(x1, Card('3'));
  std::vector<PlayPattern*> nxt3 = x0.searchMoves(&s2);
  for(unsigned i=0; i<nxt3.size(); ++i) {
    std::cout << nxt3[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt3);
  
  Trio s3(x1, Card('3'));
  std::vector<PlayPattern*> nxt4 = x0.searchMoves(&s3);
  for(unsigned i=0; i<nxt4.size(); ++i) {
    std::cout << nxt4[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt4);

  Airplane s4(x1, Card('3'), Card('4'));
  std::vector<PlayPattern*> nxt5 = x0.searchMoves(&s4);
  for(unsigned i=0; i<nxt5.size(); ++i) {
    std::cout << nxt5[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt5);

  CardArray s5k;
  s5k.push_back(Card('5'));
  s5k.push_back(Card('6'));
  AirplaneWithKicker s5(x1, Card('3'), Card('4'), s5k);
  std::vector<PlayPattern*> nxt7 = x0.searchMoves(&s5);
  for(unsigned i=0; i<nxt7.size(); ++i) {
    std::cout << nxt7[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt7);

  FourWithDualSolo s6(x1, Card('3'), Card('4'), Card('5'));
  std::vector<PlayPattern*> nxt8 = x0.searchMoves(&s6);
  for(unsigned i=0; i<nxt8.size(); ++i) {
    std::cout << nxt8[i]->print() << " | " ;
  }
  std::cout << "\n" << std::endl;
  PlayPattern::freePatternsInVector(nxt8);

  return 0;
}
