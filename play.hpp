#ifndef _PLAY
#define _PLAY

#include "game.hpp"
#include <vector>
#include <iostream>

class ChainInfo {
public:
  ChainInfo(int bb, int ee);
  ChainInfo(const ChainInfo &c);
  int beginRank;
  int endRank;
};


class PlayPattern {
public:
  PlayPattern(Hand &s) : source(s) {}
  virtual ~PlayPattern() {}
  void play();

  // Find all possible patterns in the hand of an opponent that can beat the current pattern 
  virtual std::vector<PlayPattern*> next(Hand &h);
 
  virtual std::string print();

  virtual PlayPattern* clone(Hand &s) const = 0;

  virtual unsigned int totalNumOfCards() = 0;

  static bool uniqueCards(const CardArray &arr);
  // Passing an array of sorted cards, find the longest chains in it,
  // where "2" and colored/blackwhite Jokers are not counted
  static std::vector<ChainInfo> longestChains(const CardArray &arr, unsigned int minimalLength, int m=1);

  // Find a number of unique cards prossibly with some cards excluded from being collected
  static std::vector<CardArray> findUniqueCards(const CardArray &arr, unsigned int n, int m, const std::vector<int> &excludedRanks);

  static void freePatternsInVector(std::vector<PlayPattern*> &v) {
    for(std::vector<PlayPattern*>::iterator it=v.begin(); it!=v.end(); ++it) {
      delete *it;
    }
  }
protected:
  virtual CardArray expand() const = 0;
  static std::vector<ChainInfo> allChains(Hand &h, int minimalLength, int m);
  
  Hand &source;
};


class PassPattern : public PlayPattern {
public:
  PassPattern(Hand &s) : PlayPattern(s) {}
  virtual std::vector<PlayPattern *> next(Hand &h) {
    std::cout << "next() should not explicitly called on PassPattern instances" << std::endl;
    exit(1);
  }
  virtual ~PassPattern() {}
  virtual PlayPattern* clone(Hand &s) const { return new PassPattern(s); }
  virtual unsigned int totalNumOfCards() { return 0; }
protected:
  virtual CardArray expand() const {
    std::cout << "expand() should not explicitly called on PassPattern instances" << std::endl;
    exit(1);
  }
};


/* A solo card pattern */
class Solo : public PlayPattern {
public:
  Solo(Hand &s, const Card &c) : PlayPattern(s) {
    rank = c.getRank();
  }
  virtual ~Solo() {}
  virtual std::vector<PlayPattern *> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new Solo(s, Card(rank));
  }
  virtual unsigned int totalNumOfCards() { return 1; }
  static std::vector<PlayPattern *> allSolos(Hand &h);
  int getRank() const { return rank; }
protected:
  virtual CardArray expand() const {
    CardArray arr;
    arr.push_back(Card(rank, 1));
    return arr;
  }
  /* The rank of the solo card */
  int rank;
};


/* A pair pattern of a card; e.g. 44, KK */
class Pair : public PlayPattern {
public:
  Pair(Hand &s, const Card &c) : PlayPattern(s) {
    rank = c.getRank();
  }
  virtual ~Pair() {}
  virtual std::vector<PlayPattern *> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new Pair(s, Card(rank));
  }
  virtual unsigned int totalNumOfCards() { return 2; }
  static std::vector<PlayPattern *> allPairs(Hand &h);
  int getRank() const { return rank; }
protected:
  virtual CardArray expand() const {
    CardArray arr;
    arr.push_back(Card(rank, 2));
    return arr;
  }

  int rank;
};


/* A trio pattern; e.g. 444 */
class Trio : public PlayPattern {
public:
  Trio(Hand &s, const Card &c) : PlayPattern(s) {
    rank = c.getRank();
  }
  virtual ~Trio() {}
  virtual std::vector<PlayPattern *> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new Trio(s, Card(rank));
  }
  virtual unsigned int totalNumOfCards() { return 3; }
  static std::vector<PlayPattern *> allTrios(Hand &h);
  int getRank() const { return rank; }
protected:
  virtual CardArray expand() const {
    CardArray arr;
    arr.push_back(Card(rank, 3));
    return arr;
  }
  /* The card rank */
  int rank;
};


/* A bomb consisting of four cards of the same rank */
class Bomb : public PlayPattern {
public:
  Bomb(Hand &s, const Card &c) : PlayPattern(s) {
    rank = c.getRank();
  }
  virtual ~Bomb() {}
  virtual std::vector<PlayPattern *> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new Bomb(s, Card(rank));
  }
  virtual unsigned int totalNumOfCards() { return 4; }
  static std::vector<PlayPattern *> allBombs(Hand &h);
  int getRank() const { return rank; }
protected:
  virtual CardArray expand() const {
    CardArray arr;
    arr.push_back(Card(rank, 4));
    return arr;
  }
  /* The card rank */
  int rank;
};


/* A solo chain (straight) representing an array of cards with consecutive ranks */
/* The length is at least 5. E.g. 45678 */
class SoloChain : public PlayPattern {
public:
  // ss: beginning card of the chain; ee: ending card of the chain
  SoloChain(Hand &s, const Card &ss, const Card &ee);
  ~SoloChain() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new SoloChain(s, Card(beginRank), Card(endRank-1));
  }
  virtual unsigned int totalNumOfCards() { return length; }
  static std::vector<PlayPattern*> allSoloChains(Hand &h);
  int getBeginRank() const { return beginRank; }
  unsigned int getLength() const { return length; }
protected:
  virtual CardArray expand() const;
  // The solo chain denotes the cards whose ranks are [beginRank, endRank)
  int beginRank;
  int endRank;
  unsigned int length;
};


/* A pair chain (pair straight) */
/* The length is at least 3. E.g. 66778899 */
class PairChain : public PlayPattern {
public:
  PairChain(Hand &s, const Card &ss, const Card &ee);
  virtual ~PairChain() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new PairChain(s, Card(beginRank), Card(endRank-1));
  }
  virtual unsigned int totalNumOfCards() { return length*2; }
  static std::vector<PlayPattern*> allPairChains(Hand &h);
  int getBeginRank() const { return beginRank; }
  unsigned int getLength() const { return length; }
protected:
  virtual CardArray expand() const;
  // The pair chain denotes the cards whose ranks are [beginRank, endRank)
  int beginRank;
  int endRank;
  unsigned int length;
};


/* Trio with one kicker card or a pair kicker cards */
/* E.g. 3334 or 66633 */
class TrioWithKicker : public Trio {
public:
  TrioWithKicker(Hand &s, const Card &master, const Card &k, bool flag=false);
  virtual ~TrioWithKicker() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new TrioWithKicker(s, Card(rank), Card(kickerRank), pairKicker);
  }
  virtual unsigned int totalNumOfCards() { return ( 3 + (pairKicker? 2:1) ); }
  bool singleKicker() const { return !pairKicker; }
  bool doubleKicker() const { return pairKicker; }
  static std::vector<PlayPattern*> allTriosWithKicker(Hand &h);
protected:
  virtual CardArray expand() const;

  int kickerRank;
  bool pairKicker; // indicator if having a pair kicker cards
};


/* Airplane consisting consecutive ranks of cards, each rank having 3 cards */
/* The lenght is at least 2. E.g. 444555 */
class Airplane : public PlayPattern {
public:
  Airplane(Hand &s, const Card &ss, const Card &ee);
  Airplane(const Airplane &c);
  virtual ~Airplane() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new Airplane(s, Card(beginRank), Card(endRank-1));
  }
  virtual unsigned int totalNumOfCards() { return 3*length; }
  static std::vector<PlayPattern*> allAirplanes(Hand &h);
  std::vector<int> getRankVect() const { return this->expand().getRankVect(); }
  int getLength() const { return length; }
  int getBeginRank() const { return beginRank; }
protected:
  virtual CardArray expand() const;
  // Consisting cards whose ranks are [beginRank, endRank)
  int beginRank;
  int endRank;
  unsigned int length;
};


/* Airplane with kicker cards */
/* Small kicker: airplane with the same number of unique solo kicker cards as its length. */
/*               E.g. 3334447J */
/* Large kicker: airplane with the same number of unique paired kicker cards as its length. */
/*               E.g. 33344477JJ */
/* Note small kickers which include pairs like 33344477 and 333444555667 are also allowed */
class AirplaneWithKicker : public Airplane {
public:
  AirplaneWithKicker(Hand &s, const Card &ss, const Card &ee, const CardArray &kk, bool largeKickerFlag=false);
  AirplaneWithKicker(const Airplane &a, const CardArray &kk);
  virtual ~AirplaneWithKicker() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new AirplaneWithKicker(s, Card(beginRank), Card(endRank-1), kickers, largeKicker);
  }
  virtual unsigned int totalNumOfCards() { return ( 3 + (largeKicker? 2 : 1) ) * length; }
  static std::vector<PlayPattern*> allAirplanesWithKicker(Hand &h);
  bool hasLargeKicker() const { return largeKicker; }
protected:
  virtual CardArray expand() const;

  CardArray kickers;
  bool largeKicker;
};


/* Four with dual solo cards. 444478 */
class FourWithDualSolo : public PlayPattern {
public:
  FourWithDualSolo(Hand &s, const Card &ss, const Card &k1, const Card &k2);
  FourWithDualSolo(Hand &s, const Card &ss, const Card &k1);
  virtual ~FourWithDualSolo() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new FourWithDualSolo(s, Card(rank), Card(kickerRank[0]), Card(kickerRank[1]));
  }
  virtual unsigned int totalNumOfCards() { return 6; }
  static std::vector<PlayPattern*> allFourWithDualSolo(Hand &h);
  int getRank() const { return rank; }
protected:
  virtual CardArray expand() const;

  int rank;
  int kickerRank[2];
};


/* Four with dual pair cards. E.g. 44447788 */
class FourWithDualPair : public PlayPattern {
public:
  FourWithDualPair(Hand &s, const Card &ss, const Card &k1, const Card &k2);
  virtual ~FourWithDualPair() {}
  virtual std::vector<PlayPattern*> next(Hand &h);
  virtual PlayPattern* clone(Hand &s) const {
    return new FourWithDualPair(s, Card(rank), Card(kickerRank[0]), Card(kickerRank[1]));
  }
  virtual unsigned int totalNumOfCards() { return 8; }
  static std::vector<PlayPattern*> allFourWithDualPair(Hand &h);
  int getRank() const { return rank; }
protected:
  virtual CardArray expand() const;

  int rank;
  int kickerRank[2];
};


/* Not fully implemented */
class SpaceShuttle : public PlayPattern {
public:
  SpaceShuttle(Hand &s, const Card &ss, const Card &ee);
  virtual ~SpaceShuttle() {}
  virtual PlayPattern* clone(Hand &s) const {
    return new SpaceShuttle(s, Card(beginRank), Card(endRank-1));
  }
  virtual unsigned int totalNumOfCards() { return length*4; }
protected:
  virtual CardArray expand() const;

  int beginRank;
  int endRank;
  unsigned int length;
};


/* Not fully implemented */
class SpaceShuttleWithKicker : public SpaceShuttle {
public:
  SpaceShuttleWithKicker(Hand &s, const Card &ss, const Card &ee, const CardArray &arr);
  virtual ~SpaceShuttleWithKicker() {}
  virtual PlayPattern* clone(Hand &s) const {
    return new SpaceShuttleWithKicker(s, Card(beginRank), Card(endRank-1), kickers);
  }
  virtual unsigned int totalNumOfCards() { return length*4+kickers.totalNumOfCards(); }
protected:
  virtual CardArray expand() const;
private:
  bool dualPairKicker() {
    return this->length == kickers.size();
  }
  
  CardArray kickers;
};


/* Rocket: two cards consisting of a colored and a whiteblack jockers */
class Rocket : public PlayPattern {
public:
  Rocket(Hand &s) : PlayPattern(s) {}
  virtual ~Rocket() {}
  // Passing a sorted hand of cards by their ranks in ascending order,
  // check if the largest two cards are jokers or not
  static std::vector<PlayPattern*> findRocket(Hand &h);
  virtual std::vector<PlayPattern*> next(Hand &h) { return std::vector<PlayPattern*>(); }
  virtual PlayPattern* clone(Hand &s) const {
    return new Rocket(s);
  }
  virtual unsigned int totalNumOfCards() { return 2; }
protected:
  virtual CardArray expand() const;
};

#endif
