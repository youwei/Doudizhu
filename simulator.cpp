#include "simulator.hpp"
#include <cstdlib>
#include <cstddef>
#include <iostream>

void GameSimulator::releasePatterns(std::vector<PlayPattern*> &p){
  for(std::vector<PlayPattern*>::iterator it=p.begin(); it!=p.end(); ++it) {
    delete *it;
  }
}


int GameSimulator::currentPlayerIdWinMove(std::vector<PlayPattern*> &possibleMoves) {
  int i=0;
  unsigned int s = statePt[currentPlayerId]->totalNumOfCards();
  for(std::vector<PlayPattern*>::iterator it=possibleMoves.begin(); it!=possibleMoves.end(); ++it) {
    if(s == (*it)->totalNumOfCards()) {
      return i; 
    }
    ++i;
  }
  return -1;
}


int UniformSimulator::run() {
  while(1) {
    // If neither of the other players beat, the leading player arbitrarily takes another action
    if(currentPlayerId == beginPlayerId) {
      std::vector<PlayPattern*> allPossiblePatterns = statePt[currentPlayerId]->searchMoves();
      if (prevPattern != NULL) {
	delete prevPattern;
      }

      int a = choosePlayPattern(allPossiblePatterns);
      prevPattern = allPossiblePatterns[a]->clone(*statePt[currentPlayerId]);
      GameSimulator::releasePatterns(allPossiblePatterns);
 
      prevPattern->play();
      //std::cout << prevPattern->print() << std::endl;

      // If no cards left after playing the pattern, return current player as winner
      if(statePt[currentPlayerId]->totalNumOfCards()==0) {
	delete prevPattern;
	prevPattern = NULL;
        //std::cout << "player " << currentPlayerId <<" wins." << std::endl;
	return currentPlayerId;
      }

      currentPlayerId = (currentPlayerId+1)%statePt.size();
    }

    std::vector<PlayPattern*> moves = statePt[currentPlayerId]->searchMoves(prevPattern);
    if(moves.size() != 0) {
      // Consider the pass action first; if it is not, then draw an arbitrary pattern
      int a = rand()%(moves.size()+1)==0? -1 : choosePlayPattern(moves);
      // If the chosen is not the "pass" pattern, then beat the previous move with it
      if(a >= 0) {
	delete prevPattern;
	prevPattern = moves[a]->clone(*statePt[currentPlayerId]);
	GameSimulator::releasePatterns(moves);

        //std::cout << prevPattern->print() << std::endl;
	prevPattern->play();
	beginPlayerId = currentPlayerId;

	if(statePt[beginPlayerId]->totalNumOfCards()==0) {
	  delete prevPattern;
	  prevPattern = NULL;

	  //std::cout << "player " << beginPlayerId << " wins." << std::endl;
	  return beginPlayerId;
	}
      }
      else {
	GameSimulator::releasePatterns(moves);
        //std::cout << "player " << currentPlayerId << " passed." << std::endl;
      }

    }
    else {
      //std::cout << "player " << currentPlayerId << " cannot beat." << std::endl;
    }

    currentPlayerId = (currentPlayerId+1)%statePt.size();
  }

}


int main3() {
  Hand ll(0), p1(1), p2(2);
  ll.addCard('3', 3);
  ll.addCard('4', 3);
  ll.addCard('T', 1);
  ll.addCard('J', 1);
  ll.addCard('r', 1);

  p1.addCard('5',2);
  p1.addCard('6',2);
  p1.addCard('7',2);
  p1.addCard('R',1);

  p2.addCard('Q',2);
  p2.addCard('K',2);

  srand(time(0));
  std::vector<Hand*> pst_2p, pst_3p;

  pst_3p.push_back(&ll);
  pst_3p.push_back(&p1);
  pst_3p.push_back(&p2);
  UniformSimulator s(pst_3p, 0, 0, NULL);
  s.run();

  pst_2p.push_back(&ll);
  pst_2p.push_back(&p1);
  UniformSimulator s1(pst_2p, 0, 0, NULL);
  s1.run();

  return 0;
}
