#ifndef _SIMULATOR
#define _SIMULATOR

#include <cassert>
#include "game.hpp"
#include "play.hpp"
#include <vector>
#include <cstddef>
#include <cassert>

class GameSimulator;
class UniformSimulator;

class GameSimulatorAbstractFactory {
public:
  GameSimulatorAbstractFactory() {}
  virtual ~GameSimulatorAbstractFactory() {}
  virtual GameSimulator *createGameSimulator(const std::vector<Hand*> &ps,
				     int initId, int currentId, const PlayPattern *prev) = 0;
};



class GameSimulator {
public:
  GameSimulator(const std::vector<Hand*> &ps, int initId, int currentId, const PlayPattern *prev) {
    for(unsigned int i=0; i<ps.size(); ++i) {
      statePt.push_back(new Hand(*ps[i]));
    }
    beginPlayerId = initId;
    currentPlayerId = currentId;

    if(prev != NULL) {
      prevPattern = prev->clone(*statePt[initId]);
    }
    else {
      prevPattern = NULL;
    }
    
  }

  virtual ~GameSimulator() {
    for(unsigned int i=0; i<statePt.size(); ++i) {
      delete statePt[i];
    }
    if(prevPattern != NULL) {
      delete prevPattern;
    }
  }
  virtual int run()=0;

protected:
  // Given the possible moves by the current player, find if there is a move that leads him to win;
  // namely, no card is left in his hand after this action.
  int currentPlayerIdWinMove(std::vector<PlayPattern*> &possibleMoves);

  static void releasePatterns(std::vector<PlayPattern*> &p);

  std::vector<Hand*> statePt;

  // The player who starts a round
  int beginPlayerId;
  // The player who is about to play or follow cards
  int currentPlayerId;
  // The play pattern for the current player to beat or follow,
  // which is passed and deeply copied from the previous player
  PlayPattern *prevPattern;
};


class UniformSimulator : public GameSimulator {
public:
  UniformSimulator(const std::vector<Hand*> &ps, int initId, int currentId, const PlayPattern *prev)
    : GameSimulator(ps, initId, currentId, prev) {}
  virtual ~UniformSimulator() {}
  virtual int run();
private:
  // If exists a pattern that can win the game, then return it.
  // Otherwise, return an arbitrary pattern
  int choosePlayPattern(std::vector<PlayPattern*> &patterns) {
    int a0 = currentPlayerIdWinMove(patterns);
    int a;
    if(a0>=0) {
      a = a0;
    }
    else {
      a = rand() % patterns.size();
    }
    return a;
  }
};


class UniformSimulatorFactory : public GameSimulatorAbstractFactory {
public:
  virtual GameSimulator *createGameSimulator(const std::vector<Hand*> &ps,
                                             int initId, int currentId, const PlayPattern *prev) {
    return new UniformSimulator(ps, initId, currentId, prev);
  }
};


#endif
